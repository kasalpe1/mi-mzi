\documentclass{article}

    % Basic figure setup, for now with no caption control since it's done
    % automatically by Pandoc (which extracts ![](path) syntax from Markdown).
    \usepackage{graphicx}
    \usepackage{adjustbox} % Used to constrain images to a maximum size 
    \usepackage{enumerate} % Needed for markdown enumerations to work
    \usepackage{geometry} % Used to adjust the document margins
    \usepackage{amsmath} % Equations
    \usepackage{amssymb} % Equations
	\usepackage{algorithm}
    \usepackage[noend]{algpseudocode} 
    \usepackage[utf8x]{inputenc} % Allow utf-8 characters in the tex document
    \usepackage{csquotes}
    \usepackage[hidelinks]{hyperref}
    \usepackage[czech]{babel}
    \title{Gradientní sestup a Adam}
  	\author{Petr Kasalický}
    \begin{document}
    
    \maketitle

    
	\section{Úvod}
	Tato práce má za cíl uvést čtenáře do problematiky optimalizace metodou gradientního sestupu a ukázat, jak bývá v praxi aplikována v neustále rozvíjejícím se oboru strojového učení. Dále je poukázáno na některé nedostatky této techniky a je představena optimalizační metoda \textit{Adam}, která tyto problémy částečně řeší. Na konci je proveden experiment ukazující situaci, ve které použití metody \textit{Adam} vede k rychlejší konvergenci oproti gradientnímu sestupu. Vzhledem ke stáří této metody a vůbec celého odvětví ještě nejsou ustáleny české ekvivalenty všech použitých termínů, čili budeme spolu s překlady uvádět v závorkách i originální (většinou) anglické označení.
	
		
	
	\section{Učení s učitelem a gradientní sestup}
	V posledních letech zažilo obrovský rozmach odvětví strojového učení (\textit{machine learning}), konkrétně jeho podoblast zvaná učení s učitelem (\textit{supervised learning}), jejíž cílem je naučit se transformovat vstupní data na správný výstup. Tato transformace je reprezentovaná funkcí $f: \mathbb{R}^p \rightarrow \mathbb{R}$. Vektor $\overrightarrow{x} = (x_1, x_2, \dots, x_p)$ představuje vstupní data a správný výstup označme jako $y$. Funkci $f$ se snažíme odhadnout na základě dat, tedy dvojic $(\overrightarrow{x}, y)$. Těchto dvojic máme typicky více, proto zavádíme označení:
	$$\mathbb{X} = 
	\begin{pmatrix} 
	x_{1,1} & x_{1,2} & \dots & x_{1,p} \\
	x_{2,1} & x_{2,2} & \dots & x_{2,p} \\
	\vdots & \vdots & \ddots & \vdots \\
	x_{n,1} & x_{n,2} & \dots & x_{N,p} \\
	\end{pmatrix}
	\hspace{25px}\text{a}\hspace{25px} \mathbb{Y} = 
	\begin{pmatrix} \label{key}
	y_1 \\
	y_2 \\
	\vdots \\
	y_n
	\end{pmatrix}
	$$
	a $\overrightarrow{x_i} = \mathbb{X}_{i, :}$, tedy řádek reprezentuje jednu instanci dat se všemi svými atributy (\textit{features}).
	
	Jak dobře funkce $f$ predikuje cílovou proměnnou lze vyjádřit pomocí tzv. ztrátové funkce (\textit{loss function}) $L$ jako $L(y, f(\overrightarrow{x}))$. Kdyby byla funkce $f$ naprosto obecná, tak se úplně přizpůsobí datům, tzn. pro $\forall i \in \{1, \dots, N\}:  L(y_i, f(\overrightarrow{x_i})) = 0$, což je jev, kterému se říká přeučení (\textit{overfitting}) a má za důsledek špatnou generalizační schopnost $f$. Také by bylo těžké pro nás takto obecnou funkci uchopit a přizpůsobovat ji datům (učit ji). Řešením je vybrat si podmnožinu funkcí (říkejme jim třídy modelů) s konečným počtem parametrů (říkejme jim váhy), které nastavíme tak, aby byla hodnota  $$\frac{1}{N}\sum_{i=1}^{N}L(y_i, f(\overrightarrow{x}_i))$$
	co nejmenší. Jedná se tedy o minimalizaci ztrátové funkce s ohledem na váhy. Pro některé kombinace třídy modelu a konkrétní ztrátové funkce existuje explicitní předpis pro váhy. Příkladem je lineární regrese, definovaná jako $f(\overrightarrow{x}_i) = w_0 + x_1w_1 + x_2w_2 + \dots + x_pw_p$, v kombinaci s kvadratickou ztrátovou funkcí $L(y_i, f(\overrightarrow{x}_i)) = (y_i - f(\overrightarrow{x}_i))^2$, jejíž vektor vah $\overrightarrow{w}$ lze odhadnout výrazem 	
	$$\overrightarrow{w} = (\mathbb{X}_0^T\mathbb{X}_0)^{-1}\mathbb{X}_0^T\mathbb{Y},$$
	kde $\mathbb{X}_0 \in \mathbb{R}^{N,p+1}$ je původní matice $\mathbb{X}$, akorát byl zleva přidán sloupec jedniček simulující roli $x_0$ pro $w_0$.
	
	Obecně ale explicitní předpis neexistuje a je potřeba nastavit správné váhy jiným způsobem. Níže postupy jsou obecné metody optimalizace, které nacházejí použití ve spoustě odvětvích, avšak my je nebudeme představovat v plné obecnosti, nýbrž ukážeme jejich aplikaci ve strojovém učení. Pravděpodobně nejpoužívanější je metoda gradientního sestupu (\textit{gradient descent}). Ta vyžaduje, aby ztrátová funkce $L$ s dosazenou funkcí $f$ obsahující váhy $(w_1, w_2, \dots, w_m)$ byla diferencovatelná, neboť využívá skutečnosti, že její gradient podle všech vah určuje směr nejrychlejšího růstu funkce. Gradient záporné funkce pak určuje směr nejrychlejšího poklesu. Jedná se o iterační metodu, kdy je v jednotlivých krocích spočtena hodnota gradientu pro aktuální bod a použita pro aktualizaci vah, konkrétně pro vektor vah $\overrightarrow{w} = (w_1, w_2, \dots, w_m)$ a jeden bod $\overrightarrow{x_i}$ s příslušejícím výstupem $y_i$ je proveden následující krok:	
	$$\overrightarrow{w} \leftarrow \overrightarrow{w} - \alpha \nabla_wL(y_i, f(\overrightarrow{x_i})),$$
	kde parametr $\alpha$ se nazývá učící koeficient (\textit{learning rate}) a upravuje velikost změny. Nastavení toho parametru může být netriviální, pro malé hodnoty může algoritmus skončit v lokálním minimum, pro velké zase přestřelit a nikdy nezkonvergovat. Na velikosti kroku se také podílí velikost (\textit{magnitude}) gradientu. Výraz $\nabla_wL(y_i, f(\overrightarrow{x_i})$ je vektor o dimenzi $m$, vynásobením číslem $\alpha$ se rozměr nezmění, $\overrightarrow{w}$ je také vektor o dimenzi $m$, čili se jedná o rozdíl dvou vektorů provedený po složkách. A právě členy gradientu vyjádřují kromě směru také velikost kroku v jednotlivých směrech. \cite{kathuria_intro_2018}

	Pro více dat může mít gradientní sestup podle \cite{brownlee_gentle_2017} tři podoby označované přivlastky: stochatický (\textit{stochastic}), dávkový (\textit{batch}) a mini-dávkový (\textit{mini-batch)}. Stochastický provádí aktualizaci pro každou dvojici  $(\overrightarrow{x}_i, y_i)$  jak je znázorněno v algoritmu~\ref{alg:sgd}. Tato varianta je velmi jednoduchá a může pomoci vyhnout se předčasné konvergenci v lokálním minimu, ale také je výpočetně náročná.

\begin{algorithm}
	\caption{Stochastický gradientní sestup} \label{alg:sgd}
	\begin{algorithmic}[1]
		\Statex
		\While{$L(y, f(x))$ nezkonvergovala}
			\For{$i \gets 1 \textrm{ to } N$}
			\State $\overrightarrow{w} \leftarrow \overrightarrow{w} - \alpha \nabla_wL(y_i, f(\overrightarrow{x_i}))$
			\EndFor
		\EndWhile
	\end{algorithmic}
\end{algorithm}
	Druhou variantou gradientního sestupu je sečíst chyby všech $N$ instancí dat a až pak aktualizovat váhy, jak je ilustrováno v algoritmu~\ref{alg:bgd}. Cyklu, kdy jsou použita všechna data, se říká iterace nebo také epocha. Je tedy provedena jedna aktualizace za každou epochu. Tento postup je sice výpočetně efektivnější, ale je zase paměťově náročnější a může předčasně konvergovat.
\begin{algorithm}
	\caption{Dávkový gradientní sestup} \label{alg:bgd}
	\begin{algorithmic}[1]
		\Statex
		\While{$L(y, f(x))$ nezkonvergovala}
		\State $e  \leftarrow 0$
		\For{$i \gets 1 \textrm{ to } N$}
			\State $e \leftarrow e + L(y_i, f(\overrightarrow{x_i}))$
		\EndFor
		\State $\overrightarrow{w} \leftarrow \overrightarrow{w} - \alpha \nabla_we$
		
		\EndWhile
	\end{algorithmic}
\end{algorithm}

	Třetí variantou je pak kompromis mezi dvěma výše uvedenými, jehož pseudokód nalezneme v algoritmu~\ref{alg:mbgd}. Hyperparametrem \textit{batch-size} je určen počet instancí, jejichž chyba se naakumuluje před provedením aktualizace. Dataset je tedy rozdělen na části obsahující \textit{batch-size} záznamů. Pokud počet dat $N$ není násobkem \textit{batch-size}, tak poslední část je ponechána menší. Empiricky zjištěnou doporučenou hodnotou toho hyperparametru je 16, 32, 64 nebo i 128. 
\begin{algorithm}
	\caption{Mini-dávkový gradientní sestup} \label{alg:mbgd}
	\begin{algorithmic}[1]
		\Statex
		\State $e  \leftarrow 0$
		\While{$L(y, f(x))$ nezkonvergovala}
			\For{$i \gets 1 \textrm{ to } N$}
			\State $e  \leftarrow  e +L(y_i, f(\overrightarrow{x_i}))$
			\If{$i$ je celočíselným násobkem \textit{batch-size} nebo $i = N$}
				\State $\overrightarrow{w} \leftarrow \overrightarrow{w} - \alpha \nabla_we$			\State $e  \leftarrow 0$	
			\EndIf
			\EndFor
		\EndWhile
	\end{algorithmic}
\end{algorithm}
\section{Nedostatky gradientního sestupu}
	Jedním z problémů, se kterými se potýká gradientní sestup, je podle \cite{kathuria_intro_2018-1} stav nazývaný chorobné zakřivení (\textit{pathological curvature}). Na tento jev neexistuje přesná definice, ale ilustrace na obrázku~\ref{fig:chorobne_zakriveni} ukazuje, že se jedná se o jakési kličkování nad údolím. Jev se vyskytuje hlavně u neuronových sítí, které mají miliony vah k nastavení a tedy velmi složitou strukturu optimalizované funkce, a může způsobit velmi pomalou konvergenci. Přičinou takového přeskakování sem a tam je skutečnost, že gradientní sestup jakožto zástupce optimalizace prvního řádu (\textit{first order optimalization}) bere v potaz jen a pouze jen první derivaci udávající směr největšího růstu. Existují metody vyšších řádů řešící tento problém, ovšem za cenu vysoké výpočetní náročnosti. S cílem udržet stávající náročnost tak vznikly metody založené na gradientním sestupu, které pomáhají tento problém redukovat. Příkladem je gradientní sestup s momentem, RMSProp (\textit{Root Mean Square Propagation}) a Adam, který kombinuje princip první dvou zmíněných.
	
	\begin{figure}[ht!]\centering
		\includegraphics[width=0.5\textwidth]{chorobne_zakriveni}
		\caption{Chorobné zakřivení \cite{kathuria_intro_2018-1}}\label{fig:chorobne_zakriveni}
	\end{figure}

\section{Adam}
	Metoda Adam neboli \textit{Adaptive Moment Optimization} byla publikována v roce 2014 v článku \cite{kingma_adam:_2014}, ze kterého budeme spolu s \cite{kathuria_intro_2018-1} v této sekci čerpat. Tři přístupy pro práci s více daty představené v u gradientního sestupu je možné aplikovat i zde. Proto si vyberme ten nejjednodušší, totiž Stochastický gradientní sestup, a aplikujme na něj aktualizační krok představený metodou Adam. Oproti dříve ukázaným algoritmům je zde potřeba explicitně znát počet aktualizačních kroků, které jsme již udělali. Zavedeme se na to proměnnou $t$, která se v každém kroku inkrementuje. Pojďme si nyní algoritmus~\ref{alg:adam} rozebrat a ukázat, co dělá který řádek.
	
\begin{algorithm}
	\caption{Adam} \label{alg:adam}
	\begin{algorithmic}[1]
		\Statex
		\State $\alpha = 0.001$ \Comment{Velikost kroku}
		\State $\beta_1 = 0.9$ \Comment{Koeficient exponenciálního poklesu}
		\State $\beta_2 = 0.999$ \Comment{Koeficient exponenciálního poklesu}
		\State $\epsilon = 10^{-8}$ 
		\State $u_0 \leftarrow (0, 0, \dots, 0)$ \Comment{Nulový vektor dimenze $m$}
		\State $v_0 \leftarrow (0, 0, \dots, 0)$ \Comment{Nulový vektor dimenze $m$}
		\State $t \leftarrow 1$ \Comment{První časový krok}
		\While{$L(y, f(x))$ nezkonvergovala}
			\For{$i \gets 1 \textrm{ to } N$}
				\State $g_t \leftarrow \nabla_wL(y_i, f(\overrightarrow{x_i})$ \Comment{Gradient optimalizované funkce v bodě $\overrightarrow{w}$}
				\State $u_t \leftarrow \beta_1 \cdot u_{t-1} + (1-\beta_1)\cdot g_t$
				\State $v_t \leftarrow \beta_2 \cdot v_{t-1} + (1-\beta_2)\cdot g_t^2$ \Comment{$g_t^2 = g_t \cdot g_t$ s násobením po prvcích}	
				\State $\hat{u} \leftarrow u_t/(1-\beta_1^t)$ 
				\State $\hat{v} \leftarrow v_t/(1-\beta_2^t)$ 
				\State $\overrightarrow{w} \leftarrow \overrightarrow{w} - \alpha \cdot \hat{u}/(\sqrt{\hat{v}} + \epsilon)$
				\State $t \leftarrow t + 1$
			\EndFor
		\EndWhile
	\end{algorithmic}
\end{algorithm}

	Na řádcích $1 - 4$ nastavíme hyperparametry algoritmu. Parametr $\alpha$ má stejnou úlohu jako v gradientním sestupu, tedy spoluurčuje velikost kroku. Parametry $\beta_1$ a $\beta_2$ musí nabývat hodnot z intervalu $[0, 1)$ a určují, jak moc velkou roli při určování směru hrají předchozí kroky. Speciální případy hodnot budou ještě rozebrány dále. Posledním hyperparametrem je $\epsilon$, jehož hlavní funkcí je, aby se na řádku $15$ nemohlo stát, že výraz $(\sqrt{\hat{v}} + \epsilon)$ bude roven nule, kterou nelze dělit. Konkrétní hodnoty těchto hyperparametrů uvedené v algoritmu~\ref{alg:adam} byly experimentálně nalezeny při řešení testovacích úloh uvedených v \cite{kingma_adam:_2014}, což ovšem neznamená, že budou dobře fungovat na všech optimalizačních úlohách.
	
	Dále jsou na řádcích $5-7$ inicializovány vektory $u$, $v$ a čítač $t$. Ve vektoru $u$ jsou akumulovány hodnoty gradientu v předešlých krocích vynásobené koeficientem $\beta_1$. Konkrétně se jedná se o exponenciální klouzavý průměr (\textit{Exponential Moving Average}) gradientu, jehož hodnotu v čase $t$ si odvodíme. Začneme rozepsáním $u_4$ a objevený vzoreček zobecníme. 
	\begin{align}
		u_4 &= \beta_1 \cdot u_3 + (1-\beta_1) \cdot g_4 \nonumber \\
		&= \beta_1 \cdot (\beta_1 \cdot u_2 + (1-\beta_1) \cdot g_3) + (1-\beta_1) \cdot g_4 \nonumber \\
		&= \beta_1 \cdot (\beta_1 \cdot (\beta_1 \cdot u_1 + (1-\beta_1) \cdot g_2) + (1-\beta_1) \cdot g_3) + (1-\beta_1) \cdot g_4 \nonumber \\
		&= \beta_1 \cdot (\beta_1 \cdot (\beta_1 \cdot ( \beta_1 \cdot u_0 + (1-\beta_1) \cdot g_1) + (1-\beta_1) \cdot g_2) + (1-\beta_1) \cdot g_3) + (1-\beta_1) \cdot g_4 \nonumber \\ 
		&= \beta_1 \cdot (\beta_1 \cdot (\beta_1 \cdot (1-\beta_1) \cdot g_1 + (1-\beta_1) \cdot g_2) + (1-\beta_1) \cdot g_3) + (1-\beta_1) \cdot g_4 \nonumber \\
		&= \beta_1 \cdot (\beta_1^2 \cdot (1-\beta_1) \cdot g_1 + \beta_1 \cdot (1-\beta_1) \cdot g_2 + (1-\beta_1) \cdot g_3) + (1-\beta_1) \cdot g_4 \nonumber \\
		&= \beta_1^3 \cdot (1-\beta_1) \cdot g_1 + \beta_1^2 \cdot (1-\beta_1) \cdot g_2 + \beta_1 \cdot (1-\beta_1) \cdot g_3 + (1-\beta_1) \cdot g_4 \nonumber \\
		&= \sum_{i = 1}^{4}\beta_1^{4-i} \cdot (1-\beta_1) \cdot g_i \nonumber \\	
		&= (1-\beta_1) \cdot \sum_{i = 1}^{4}\beta_1^{4-i} \cdot g_i \nonumber
	\end{align}
	
	z čehož lze odhadnout obecný předpis, a to $$u_t = (1-\beta_1) \cdot \sum_{i = 1}^{t}\beta_1^{t-i} \cdot g_i.$$ Jako důkaz správnosti odhadu obecného vzorce budeme brát skutečnost, že je tento vzorec (bez odvození) zmíněn v \cite{kingma_adam:_2014}. Z rozepsaného tvaru a intervalu hodnot, kterých může $\beta_1$ nabývat, můžeme vidět, že vliv $g_i$ na hodnotu $u_{i+s}$ se zmenšuje s roustoucím $s$. Také si lze všimnout, že čím větší (tzn. blíže jedničce) je $\beta_1$, tím větší dopad mají minulé hodnoty gradientu.
	
	
	Analogicky je to s vektorem $v$ a koeficientem $\beta_2$, pouze zde se nenasčítává gradient, ale jeho druhá mocnina provedená po složkách, čili předpis se změní na $$v_t = (1-\beta_2) \cdot \sum_{i = 1}^{t}\beta_2^{t-i} \cdot g_i^2.$$ 
	
	Následně je vektor $u_t$, respektive $v_t$, podělen výrazem $(1-\beta_1^t)$, respektive $(1-\beta_2^t)$, a uložen do vlastní proměnné. Důvod tohoto kroku je popsán v sekci~3 práce \cite{kingma_adam:_2014}. Jedná se o korekci odhadu, ale protože my zde statistický pohled na problematiku optimalizace od začátku nezmiňujeme, tak vysvětlení tohoto kroku přeskočíme. Jenom poukážeme, že efekt tohoto kroku se snižuje s rostoucím $t$, neboť $$\lim\limits_{t \rightarrow \infty} \frac{u}{1-\beta_1^t} = u\hspace{10px}\text{pro}\hspace{10px}\beta \in [0, 1).$$
	A jako poslední krok v cyklu je provedena skutečná aktualizace vah podobná gradientnímu sestupu. 
	
	Podíváme se na jeden speciální případ a totiž fakt, že pro $\epsilon = \beta_1 = \beta_2 = 0$ je tento algoritmus nefunkční. Platí totiž, že $u_t \leftarrow 0 \cdot u_{t-1} + (1 - 0) \cdot g_t = g_t$. Stejně tak $v_t \leftarrow g_t^2$, $\hat{u} \leftarrow u_t/(1 - 0^t) = u_t$, $\hat{v} \leftarrow v$, čímž pádem aktualizační pravidlo vypadá jako 
	\begin{align}
		\overrightarrow{w} &\leftarrow \overrightarrow{w} - \alpha \cdot \hat{u}/(\sqrt{\hat{v}} + \epsilon) \nonumber \\
		&\leftarrow 	\overrightarrow{w} - \alpha \cdot u_t/(\sqrt{v_t}) \nonumber \\
		&\leftarrow 	\overrightarrow{w} - \alpha \cdot g_t/(\sqrt{g_t^2}) \nonumber \\
		&\leftarrow 	\overrightarrow{w} - \alpha \cdot g_t/g_t \nonumber \\
		&\leftarrow 	\overrightarrow{w} - \alpha, \nonumber
	\end{align}
	tedy odečítá se pořád ta samá konstanta a vůbec nejsou reflektovány data. 
	\subsection{Ilustrační porovnání stochastického sestupu s metodou Adam}
	Nyní se pojďme podívat, jak bude Adam reagovat na výše uvedený nedostatek gradientního sestupu, tedy na chorobnou zakřivenost optimalizované funkce. Pro optimalizaci jsme zvolili funkci dvou promměných (z důvodu  vizualizaci) s předpisem 
	$$f(w_0, w_1) = (-w_0^6 + 10^4 \cdot x^2 + 1000 \cdot (y-2)^2 + 1000 \cdot y) \cdot \frac{1}{10000},$$
	jejíž graf můžeme vidět na obrázku~\ref{fig:funkce_f}. 
	
	\textit{Poznámka}: Nyní jsme se trochu odchýlili od původního značení, dříve byla optimalizovaná funkce $L(y, f(\overrightarrow{x}))$ pro trénovací data $(\overrightarrow{x}, y)$. Řekněme, že jsme pro jednoduchost tyto informace již dosadili do předpisu optimalizované funkce a přeznačili ji na $f$. Pořád ale optimalizujeme vektor vah, zde je to konkrétně $\overrightarrow{w} = (w_0, w_1)$.
	
	\begin{figure}[ht!]\centering
		\includegraphics[width=\textwidth]{funkce_f}
		\caption{Graf funkce $f$}\label{fig:funkce_f}
	\end{figure}
	
	Na obrázku~\ref{fig:funkce_f} můžeme vidět částečně chorobné zakřivení, tedy údolí mezi dvěma příkrými kopci, které v jednom směru klesá až do (lokálního) minima. Nastavme si úvodní váhy na fixní hodnotu $\overrightarrow{w} = (-7, 9)$ a sledujme průběh optimalizace za použití obou metod. U každé z nich necháme provést 20~aktualizačních kroků. 
	
	Průběhy zakreslené do grafu jsou vidět v obrázcích~\ref{fig:optimalizace_pomoci_sgd} a \ref{fig:optimalizace_pomoci_adam}. Omluvte prosím špatné pořadí funkce a šipek reprezentující aktualizační krok způsobené nedostatečnými možnostmi balíčku \textit{Matplotlib}. Šipky mají být samozřejmě v popředí a funkce v pozadí. Tabulka~\ref{tab:prubeh_optimalizace} pak obsahuje přesné hodnoty vah. Z tabulky i obrázku lze vidět výše zmíněné kličkování gradientního sestupu, které brání rychlejší konvergenci. Za povšimnutí stojí, že ačkoliv učící krok $\alpha$ byl nastaven pro oba algoritmy stejně, tak Adam dělal daleko menší kroky, čímž nepřestřelil hledané údolí. Ovšem i kdyby přestřelil, tak další krok by byl díky akumulování gradientu z dřívějších kroků výrazně menší a nedošlo by k přeskakování jako v obrázku~\ref{fig:optimalizace_pomoci_sgd}. Zdrojový kód provádějící tento experiment je přiložen ke zprávě za účelem reprodukce výsledků.
	
	\begin{figure}[ht!]\centering
		\includegraphics[width=0.8\textwidth]{optimalizace_pomoci_sgd}
		\caption{Ilustrace optimalizace pomocí gradientního sestupu}\label{fig:optimalizace_pomoci_sgd}
	\end{figure}
	\begin{figure}[ht!]\centering
		\includegraphics[width=0.8\textwidth]{optimalizace_pomoci_adam}
		\caption{Ilustrace optimalizace pomocí metody Adam}\label{fig:optimalizace_pomoci_adam}
	\end{figure}
	
	
	\begin{table}[h!] \centering
		\begin{tabular}{l | r | r | r || r | r | r} 
			& \multicolumn{3}{c||}{Gradientní sestup} & \multicolumn{3}{c}{Adam} \\
			Krok	&	$w_0$	&	$w_1$	&	$f(w_0, w_1)$ &	$w_0$	&	$w_1$	&	$f(w_0, w_1)$ \\ \hline\hline
			0	&	7.000	&	-9.0000	&	48.4351 &	7.0000	&	-9.0000	&	48.4351	\\ \hline
			1	&	3.0842	&	-6.9000	&	16.6572 &	6.0000	&	-8.0000	&	40.5344	\\ \hline
			2	&	-2.9168	&	-5.2200	&	13.1367 &	5.0280	&	-7.0039	&	31.0719	\\ \hline
			3	&	2.7901	&	-3.8760	&	10.8026 &	4.0442	&	-6.0148	&	21.7405	\\ \hline
			4	&	-2.6886	&	-2.8008	&	9.2157 &	3.0522	&	-5.0366	&	13.6828	\\ \hline
			5	&	2.6043	&	-1.9406	&	8.1102 &	2.0662	&	-4.0738	&	7.5433	\\ \hline
			6	&	-2.5325	&	-1.2525	&	7.3196 &	1.1088	&	-3.1315	&	3.5492	\\ \hline
			7	&	2.4700	&	-0.7020	&	6.7379 &	0.2095	&	-2.2157	&	1.5995	\\ \hline
			8	&	-2.4148	&	-0.2616	&	6.2968 &	-0.5962	&	-1.3330	&	1.3330	\\ \hline
			9	&	2.3655	&	0.0907	&	5.9519 &	-1.2724	&	-0.4908	&	2.1898	\\ \hline
			10	&	-2.3211	&	0.3726	&	5.6740 &	-1.7908	&	0.3031	&	3.5221	\\ \hline
			11	&	2.2807	&	0.5981	&	5.4437 &	-2.1388	&	1.0407	&	4.7611	\\ \hline
			12	&	-2.2437	&	0.7784	&	5.2483 &	-2.3203	&	1.7141	&	5.5476	\\ \hline
			13	&	2.2095	&	0.9228	&	5.0787 &	-2.3512	&	2.3164	&	5.7527	\\ \hline
			14	&	-2.1779	&	1.0382	&	4.9291 &	-2.2535	&	2.8414	&	5.4200	\\ \hline
			15	&	2.1485	&	1.1306	&	4.7950 &	-2.0509	&	3.2848	&	4.6922	\\ \hline
			16	&	-2.1211	&	1.2045	&	4.6736 &	-1.7668	&	3.6442	&	3.7531	\\ \hline
			17	&	2.0953	&	1.2636	&	4.5624 &	-1.4236	&	3.9191	&	2.7859	\\ \hline
			18	&	-2.0711	&	1.3108	&	4.4600 &	-1.0429	&	4.1112	&	1.9444	\\ \hline
			19	&	2.0482	&	1.3487	&	4.3651 &	-0.6457	&	4.2240	&	1.3340	\\ \hline
			20	&	-2.0266	&	1.3789	&	4.2766 &	-0.2521	&	4.2623	&	1.0016	\\ 
		\end{tabular}
	\caption{Porovnání vývoje vah při optimalizaci gradientním sestupem a metodou Adam}\label{tab:prubeh_optimalizace}
	\end{table}
	\section{Závěr}
	V této zprávě byly čtenáři představeny základy učení s učitelem využívající optimalizaci prvního řádu. Byl podrobně vysvětlen velmi populární algoritmus gradientního sestupu včetně jednoho z jeho nedostatku. Tento nedostatek je řešen taktéž představenou metodou zvanou Adam. Je ukázán pseudokód obou algoritmů a v přiloženém souboru je jejich implementace (i když naivní) v jazyce Python, která byla použita pro demonstraci rozdílu mezi metodami. 
	

	\clearpage
	\bibliographystyle{iso690}
	\bibliography{papers.bib}
\end{document}
